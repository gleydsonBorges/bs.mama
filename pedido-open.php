<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="pedido_open">
    <?php include 'assets/header.php'; ?>
    <section>
        <article class="main">
            <div class="selected_request">
                <a class="link" href=".\minha-conta.php">
                    <img class="icon icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                    Meus pedidos
                </a>
            </div>
            <div class="tab request">
                <p class="number"><span>Número do pedido:</span> 99999</p>
                <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                <p><span>Valor total:</span> R$ 99,00</p>
                <p><span>Status:</span> Entregue</p>
            </div>
            <div class="list_itens">
                <p>Itens no pedido:</p>
                <div class="item">
                    <img class="img" src="http://mama.ko/assets/images/produto.jpg" alt="">
                    <h4 class="title">Nome do produto em uma linha</h4>
                    <p class="qtd"><span>Quantidade:</span> 2 </p>
                    <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                    <p class="color"><span>Cor:</span> Azul Royal</p>
                    <p class="frame"><span>Valor:</span> R$ 99,00</p>
                </div>
                <div class="item">
                    <img class="img" src="http://mama.ko/assets/images/produto.jpg" alt="">
                    <h4 class="title">Nome do produto em uma linha</h4>
                    <p class="qtd"><span>Quantidade:</span> 2 </p>
                    <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                    <p class="color"><span>Cor:</span> Azul Royal</p>
                    <p class="frame"><span>Valor:</span> R$ 99,00</p>
                </div>

            </div>
        </article>
    </section>



    <?php include 'assets/footer.php'; ?>
</body>
</html>