<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="suporte">
    <?php include 'assets/header.php'; ?>
    <section class="">
        <article class="center">
            <h1 class="main_title center">Suporte</h1>
            <div class="nav_tab">
                <span tab="1" class="item active" >Dúvidas Frequentes</span>
                <span tab="2" class="item " >Política de trocas</span>
            </div>
        </article>
    </section>
    <section class="container">
        <article class="row box_tab">
            <div class="tab 1">
                <h3>Como Comprar?</h3>
                <p>Navegue pela loja virtual, apaixone-se pelas nossas lindas estampas e selecione seus produtos preferidos. Escolha o item que deseja e clique em comprar. Confira os produtos inseridos em seu carrinho de compras, adicione, remova ou continue comprando. Em seguida, preencha os dados, selecione a forma de envio e pagamento.</p>
                <p>Confira a revisão do pedido e caso queira finalizar seu pedido, clique em “Finalizar Compras”.</p>
                <p>Pronto, sua compra foi concluída! Anote o número do pedido e fique de olho em seu e-mail. Todas as informações a respeito de seu pedido serão comunicadas através do e-mail informado no cadastro.</p>

                <h3>Qual tempo de produção dos produtos</h3>
                <p>Nossos produtos são produzidos de acordo com os pedidos, isso porque os lotes de produção podem ocasionar a variação da tonalidade das cores de cada estampa. Para que você receba seu produto da melhor forma possível, pedimos um prazo de até 7 dias úteis da data de aprovação do pedido. </p>

                <h3>Como aplicar</h3>
                <p>Todos os manuais de aplicação estão disponíveis em pdf na página do produto escolhido, basta fazer o download.</p>

                <h3>E se eu precisar da ajuda de um colocador?</h3>
                <p>Você mesmo pode aplicar, é super fácil! Mas caso você não se sinta confortável ou não queira se preocupar com isso, pode procurar um colocador profissional. Colocador é como um pintor: cada um tem o seu de confiança. Temos alguns parceiros e se você precisar não deixe de nos procurar pelo e-mail de contato.</p>

                <h3>Como os produtos são embalados?</h3>
                <p>Embalamos com o maior carinho todos os produtos para que você receba sem nenhuma danificação. Todas as embalagens são personalizadas e feitas em material reciclável, para você reutilizá-las ou reciclá-las. </p>
            </div>
            <div class="tab 2">
                <h3>Posso trocar?</h3>
                <p>Se você tiver certeza que quer trocar seu produto, pode nos chamar para conversar sobre essa decisão.
                    Basta enviar um e-mail para pedepara@mamalovesyou.com.br, com o número do seu pedido em destaque.</p>
                <h3>Faremos o possível pra te ver feliz, mas...</h3>
                <p>
                    Não poderemos aceitar devoluções sem aviso prévio, produtos com defeito ou com itens faltando. <br>
                    As devoluções, quando autorizadas, devem ser feitas por completo. Tudo que foi, tem que voltar, inclusive a
                    sua cópia da nota fiscal. Se o seu produto tiver um defeito, nós avaliaremos o problema antes da restituição. Ok?
                    <br>
                    Para qualquer problema, nos comunique imediatamente. Essa política vale também se sua encomenda não
                    estiver completa ou se o produto não for o que você comprou.
                    <br>
                    Garantimos todos os pontos da nossa fabricação, mas caso aconteça realmente algum defeito, reenviaremos
                    a você um novo produto em até 7 dias sem qualquer custo.
                </p>
                <h3>Desisti, e agora?</h3>
                <p>Desistiu? Assim, sem mais nem menos? Que pena ... mas se isso aconteceu mesmo, você tem sete dias corridos para nos comunicar, a contar da data do recebimento. Depois disso, você envia o seu produto e todos os itens da embalagem (incluindo a nota fiscal), sem qualquer violação, para o endereço remetente da Mama Loves You. As despesas do envio ficam por sua conta. Se a sua devolução for analisada e nenhum problema for constatado, fazemos a devolução do dinheiro em créditos da loja ou reembolsamos o valor da compra.</p>
            </div>
        </article>
    </section>

    <?php include 'assets/footer.php'; ?>
</body>
</html>