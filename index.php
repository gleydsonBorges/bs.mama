<!DOCTYPE HTML>
<html>
	<head>
	<?php 
		include 'assets/config.php';
 
		$tituloPagina = ''. $empresa;
		$descricaoCompartilhamento = '';
		
		$urlPagina = $url;
		$imagemCompartilhamento = $images.'anuncio_demanda_08.png';
		$sessao = 'home';
	?>
<title><? echo $tituloPagina ?></title>
	<?php include 'assets/head.php'; ?>

		<meta property="og:title" content="<? echo $tituloPagina ?>" />
		<meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
		<meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

	</head>
	<body class="home" >
    <section class="home">
        <?php include 'assets/header.php'; ?>
    </section>
    <section class="slider_home">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section>
        <article class="mobile_item center ">
            <p class="text">Na Mama Loves You, acreditamos no poder da estamparia como forma de expressão para criar um mundo onde é mais divertido viver. </p>
            <img class="img responsive" src="<? echo $url?>assets/images/tb_tower.jpg" alt="">
        </article>
    </section>
    <section>
        <article class="frete">
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/truck.svg" alt="">
                <span>Frete Gratis</span>
                <p>Nas compras acima de R$ 300,00 para todo país</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/card.svg" alt="">
                <span>Parcele em até 3x sem juros</span>
                <p>Em compras no cartão</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/warranty.svg" alt="">
                <span>Garantia da Mama</span>
                <p>Não gostou do produto?</p>
                <p>Quer trocar por outro?</p>
                <p>Confira nossos termos de troca</p>
            </div>
        </article>
    </section>
    <section>
        <article class="calculator">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/calculator.svg" alt="">Calculadora</h6>
                <p class="center">Calcule quanto papel de parede você precisa</p>
                <p class="center">É como contar até três</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/one.svg" alt="">
                <span>Escolha sua estampa</span>
                <p>Escolha uma estampa entre as mais de 2 mil disponíveis na nossa coleção</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/two.svg" alt="">
                <span>Informe suas medidas</span>
                <p>Coloque as medidas de sua parede na nossa calculadora</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/three.svg" alt="">
                <span>Pronto!</span>
                <p>Nossa calculadora vai te informar quantos rolos do papel de parede escolhido você vai precisar</p>
            </div>
            <div>
                <button class="btn btn_calc upp">Calcular</button>
            </div>
        </article>
    </section>
    <section>
        <article class="two_banner b1">
            <a href="">
                <img class="img responsive" src="<? echo $url?>assets/images/tb_tower.jpg" alt="">
            </a>
            <a href="">
                <img class="img responsive" src="<? echo $url?>assets/images/tb_letters.jpg" alt="">
            </a>
        </article>
    </section>
    <section class="with_love">
        <article class="">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/love.svg" alt="">Produzidos com amor</h6>
            </div>

        </article>
        <article class="grid products">
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <button class="btn center">VER MAIS</button>
    </section>
    <section class="best_sellers">
        <article class="">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/favorite.svg" alt="">Os mais vendidos</h6>
            </div>
        </article>
        <article class="grid products">
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <button class="btn center">VER MAIS</button>
    </section>
    <section>
        <article class="two_banner b2">
            <a href="">
                <img class="img responsive" src="<? echo $url?>assets/images/tb_space.jpg" alt="">
            </a>
            <a href="">
                <img class="img responsive" src="<? echo $url?>assets/images/tb_sofa.jpg" alt="">
            </a>
        </article>
    </section>
    <section class="most_seen">
        <article class="">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/eye.svg" alt="">Os mais vistos</h6>
            </div>
        </article>
        <article class="grid products">
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <button class="btn center">VER MAIS</button>
    </section>
    <section class="news">
        <article class="">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/rocket.svg" alt="">Novidades</h6>
            </div>
        </article>
        <article class="grid products">
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <button class="btn center">VER MAIS</button>
    </section>
    <section class="suggested">
        <article class="">
            <div class="header">
                <h6 class="title"><img src="<? echo $url?>assets/images/icon/draw/you.svg" alt="">Sugeridos para você</h6>
            </div>
        </article>
        <article class="grid products">
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="/produto">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <button class="btn center">VER MAIS</button>
    </section>
    <section class="insta">
            <article>
                <div class="header">
                    <h6 class="title">Siga a Mama no Insta!</h6>
                </div>
            </article>
            <div class="gallery">
                <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
                <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_3.png" alt=""></a>
                <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_4.png" alt=""></a>
                <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_5.png" alt=""></a>
                <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
            </div>
            <p class="center account">@_mamalovesyou</p>
    </section>

	<?php include 'assets/footer.php'; ?>
	</body>
</html>