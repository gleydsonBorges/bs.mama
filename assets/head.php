	<meta charset="utf-8" />
		<meta http-equiv="content-language" content="pt-br" />
		<meta http-equiv="Content-Type" content="text/html;">
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta http-equiv="Pragma" content="no-cache">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
		<meta name="keywords" content="<? echo $Tags ?>" />
		<meta name="author" content="<? echo $empresa ?>" />
		<!--<meta property="article:author" content="<?/* echo $facebook */?>" /> VARIÁVEL ERRADA? -->
		<meta name="robots" content="index,follow" />
		<meta name="googlebot" content="index,follow" />
		<meta http-equiv="content-language" content="pt_BR" />
		<meta name="service" content="<? echo $descricaoCompartilhamento ?>" />
		<meta itemprop="name" content="<? echo $tituloPagina ?>">
		<meta itemprop="image" content="<? echo $imagemCompartilhamento ?>" />
		<meta itemprop="description" content="<? echo $descricaoCompartilhamento ?>" />
		<meta property="og:url" content="<? echo $urlPagina ?>" />
		<meta property="og:image" content="<? echo $imagemCompartilhamento ?>" />
		<meta name="twitter:title" content="<? echo $tituloPagina ?>">
		<meta name="twitter:description" content="<? echo $descricaoCompartilhamento ?>">
		<meta name="twitter:image" content="<? echo $imagemCompartilhamento ?>">
		<meta name="twitter:domain" content="<? echo $urlPagina ?>">
		<meta property="og:locale" content="pt_BR" />
		<meta property="og:site_name" content="<? echo $empresa ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="470" />
		<meta property="og:image:height" content="276" />
		<meta property="fb:app_id" content="458490640887690" />
		<meta name="twitter:card" content="summary">
		<meta name="format-detection" content="telephone=yes"/>
		<link href="<? echo $images ?>favicon.png" rel="icon" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<? echo $images ?>favicon.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="<? echo $images ?>favicon.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="<? echo $images ?>favicon.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="<? echo $images ?>favicon.png" />
		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="<? echo $url ?>assets/css/estrutura.css" media="all" />
		<link type="text/css" rel="stylesheet" href="<? echo $url ?>assets/javascript/dropzone-5.7.0/dist/min/dropzone.min.css" media="all" />

        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>





