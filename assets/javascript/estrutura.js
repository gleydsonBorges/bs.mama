/* 
	Hello, guys!! :)
	JS desenvolvido pela bs.agency em 2020. 

	Façam bom uso :)
*/

jQuery(document).ready(function() {

	// Variáveis:
		var BreakPoint = 1050;
		var WidthDevice = $(window).width();
		var heightDevice = $(window).height();
	// :Variáveis

	// Reload no resize da página:
		var screen = '';
		if (WidthDevice < BreakPoint) {
			screen = 'mobile';
		} else if (WidthDevice > BreakPoint) { 
			screen = 'desktop';
		}

		$(window).resize(function(){
			var cur_width = $(window).width();
			if(cur_width < BreakPoint && screen == 'desktop'){
				location.reload(); 
			} else if(cur_width > BreakPoint && screen == 'mobile'){
				location.reload();
			}
		});
	// :Reload no resize da página

	// Decodificando SVG:
		$('img.svg').each(function() {
			var $img = $(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');
			$.get(imgURL, function(data) { var $svg = $(data).find('svg'); 
			if(typeof imgID !== 'undefined') {$svg = $svg.attr('id', imgID);} 
			if(typeof imgClass !== 'undefined') {$svg = $svg.attr('class', imgClass+' replaced-svg');}$svg = $svg.removeAttr('xmlns:a'); 
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))} $img.replaceWith($svg);}, 'xml');
		});
	// :Decodificando SVG

	// Verificando Android ou IOS:
		function getMobileOperatingSystem() {
			var userAgent = navigator.userAgent || navigator.vendor || window.opera;
			if(/windows phone/i.test(userAgent)) { // Windows Phone
				$('.iOS').remove();
			} else if(/android/i.test(userAgent)) { // Android
				$('.iOS').remove();
			} else if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) { // iOS
				$('.Android').remove();
			}
		}getMobileOperatingSystem();
	// :Verificando Android ou IOS

	// GoogleMaps:
		// var position = [-23.5538037, -46.5431809];

		// function showGoogleMaps() {
		// 	var latLng = new google.maps.LatLng(position[0], position[1]);		
		// 	var mapOptions = { 
		// 		zoom: 16, 
		// 		streetViewControl: false, 
		// 		scaleControl: false, 
		// 		zoomControl: false, 
		// 		mapTypeId: google.maps.MapTypeId.ROADMAP, center: latLng 
		// 	};
		// 	map = new google.maps.Map(document.getElementById('GoogleMaps'), mapOptions);
		// 	marker = new google.maps.Marker({ position: latLng, map: map, draggable: false, animation: google.maps.Animation.DROP });
		// }

		// google.maps.event.addDomListener(window, 'load', showGoogleMaps);
	// :GoogleMaps


    $('.slider_home').slick({
        arrows: false,
    });
    $('.slider_moods').slick({
        arrows: false,
    });
    $('.slider_projeto').slick({
        arrows: false,
    });
    $('.slider_sobre').slick({
        arrows: false,
    });
    $('.slider_fale_conosco').slick({
        arrows: false,
    });
    $('.slider_shop_the_look').slick({
        arrows: false,
    });
    $('.slider_inspiracoes').slick({
        arrows: false,
    });



    //Anchor
    $('a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            targetOffset = $(id).offset().top;

        $('html, body').animate({
            scrollTop: targetOffset - 100
        }, 400);
    });



    if($('body').hasClass('inspiracoes_open')){

        //Slick Inspirações
        $('.inspiracoes_open .selected_product .slider-main').slick({
            asNavFor: '.inspiracoes_open .selected_product .slider-nav',
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            accessibility: false

        });

        $('.inspiracoes_open .selected_product .slider-nav').slick({
            asNavFor: '.inspiracoes_open .selected_product .slider-main',
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: true,
            arrows: false,
            focusOnSelect: true,
            infinite: true,
            centerMode: true,
            vertical: false,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {

                        slidesToShow: 3,
                        //slidesToScroll: 1,
                        dots: false,
                        centerMode: true,
                        infinite: true,
                        focusOnSelect: false
                    }
                }]
        });

    }else{
        $('.selected_product .slider-main').slick({
            asNavFor: '.selected_product .slider-nav',
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            accessibility: false

        });

        $('.selected_product .slider-nav').slick({
            asNavFor: '.selected_product .slider-main',
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: true,
            arrows: false,
            focusOnSelect: true,
            infinite: true,
            centerMode: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        vertical: false,
                        slidesToShow: 3,
                        //slidesToScroll: 1,
                        dots: false,
                        centerMode: true,
                        infinite: true,
                        focusOnSelect: false
                    }
                }]
        });
    }

    $('.colors .dot').click(function () {
        mark_off();
        $(this).addClass('checked')
    })

    $('.more_less .plus').click(function () {
        var result = $(this).closest('.more_less').find('.result'),
            new_result =  parseInt(result.text()) + 1
            result.text(new_result);
    })
    $('.more_less .minus').click(function () {
        var result = $(this).closest('.more_less').find('.result'),
            new_result =  parseInt(result.text()) - 1
        if (result.text() == 1){
            result.text(1);
            return false;
        }
        result.text(new_result);
    })

    //Remove item from cart
    $('.carrinho .grid_prod .btn_remove').click(function () {
        alert('deseja remover esse item do carrinho? :(')

    })

    //Active select input
    $('form #number_of_children').change(function() {

        if($(this).children("option:selected").val() != 0){
            $('form #age').prop('disabled',false)
            $('form label.age').css({'color':'#353535'});
            $('form #age').css({'opacity':'1'});
        }
        else{
            $('form label.age').css({'color':'#d4d4d4'});
            $('form #age').css({'opacity':'0.3'});
            $('form #age').prop('disabled',true)
        }
    })
    //Nav_tab
    $('.nav_tab .item').click(function () {

        var $Ntab = $(this).attr('tab');
        $('.nav_tab .item').removeClass('active')
        $(this).addClass('active');
        $('.box_tab .tab').addClass('hide');
        $('.box_tab .tab.'+ $Ntab).removeClass('hide');
        $('.box_tab .tab.'+ $Ntab).addClass('active');

    })






    // Funções executadas apenas na versão Desktop:
		function DesktopVersion() {


            // Fechar com a tecla Esc:
            $(document).keyup(function(e) {
                if (e.keyCode === 27) { // Tecla Esc
                    hide_drop();
                    hide_mask();
                }
            });
            // Fechar Click Mask
            $('.mask').click(function () {
                hide_drop();
                hide_mask();
            })

            // Active Menu
            if($('body').hasClass('produto') || $('body').hasClass('produtos') ) {
                $('.dropdown.produtos .link').addClass('active')
                $('.dropdown.produtos .icon_down').addClass('active')

            } else if($('body').hasClass('shop_the_look') || $('body').hasClass('shop_the_look_open')){
                $('.shop_the_look.link').addClass('active')
            }
            else if($('body').hasClass('moods') || $('body').hasClass('projeto')){
                $('.moods.link').addClass('active')
            }
            else if($('body').hasClass('inspiracoes') || $('body').hasClass('inspiracoes_open')){
                $('.inspiracoes.link').addClass('active')
            }

		}
	// :Funções executadas apenas na versão Desktop

	// Funções executadas apenas na versão Mobile:
		function MobileVersion() {

		    //Nav
		    $('.toggler').click(function () {
                $('.navbar').toggle();
                $('.close_navbar').removeClass('hide');
                show_mask()
            })
            $('.close_navbar').click(function () {
                $('.navbar').hide();
                hide_mask()
                $(this).addClass('hide')
            })
            $('.drop_content .subtitle').click(function () {
                $(this).closest('.drop_content').addClass('hide');
                return false
            })

            /*
            // Drop
            $('.btn_drop').click(function () {
                $(this).parent().find('.drop_content').removeClass('hide')
                $('.drop_content').removeClass('hide')
            })
            */


            // Frete home
            var frete = $('.frete').parent().html();
            $('.frete').remove()
            $('.two_banner.b1').after(frete)

            // Banner after
            var b2 = $('.home .b2').html();
            $('.b2').remove();
            b2 =  '<article class="two_banner b2">\n' + b2 + '        </article>'
            $('.home .with_love').after(b2)

            // Slick
            $('.insta .gallery').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                arrows: false
            });

            $('.with_love .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });
            $('.best_sellers .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });
            $('.most_seen .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });
            $('.news .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });
            $('.suggested .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });
            $('.inspiracoes_open .grid').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                centerPadding: '60px',
                variableWidth: true,
                arrows: false
            });

            if($('body').hasClass('produto'))  { // Página Inicial
                // Title Produto
                if($('.selected_product #title_produto').length){
                    var title = $('.selected_product #title_produto')[0].outerHTML;
                    $('.selected_product .title').remove()
                    $('.selected_category').after(title)
                }

                // Remove Calculator
                $('.produto .calculator').remove();

                $('.related_products .grid').slick({
                    infinite: true,
                    centerMode: true,
                    slidesToShow: 1,
                    centerPadding: '60px',
                    variableWidth: true,
                    arrows: false
                });

            } else if($('body').hasClass('produto')) {

            }


		}
	// :Funções executadas apenas na versão Mobile

	// Executando versionamentos
		if(WidthDevice <= 900) { MobileVersion(); } else { DesktopVersion(); }
	// FadeIn da página após o carregamento.
		$('body').fadeIn('slow');
	// ScrollTop
		// $('html, body').animate({scrollTop: '0px'}, 600);


	// Executando funções em páginas específicas

    if($('body').hasClass('moods') ) { // Página Moodboard

        // Selecionar Estilo
        $('.form_mood .list label').click(function () {
           if($(this).find('.mask_item').hasClass('hide')){
               $(this).find('.mask_item').removeClass('hide')
           }
           else{
               $(this).find('.mask_item').addClass('hide')
           }
       })

        //Next Tab
        $('.form_mood .btn_next').click(function () {
            var tab = $(this).closest('.tab').attr('tab')
            var next_tab = parseInt(tab) + 1

            console.log(next_tab)
            if(next_tab == 5){
                $('.form_mood .btn_send').removeClass('hide')
            }

            $(this).closest('.tab').addClass('hide')
            $('.form_mood .tab_'+ next_tab).removeClass('hide')

            $('.status .step'+ next_tab +' .dot').addClass('active')
        })

        // FILE UPLOAD
        // BICLIOTECA >> https://www.dropzonejs.com/
        $(".dropzone").dropzone({
            url: "/file/post",
            addRemoveLinks: true,
        });

        // Select Plan
        $('.moods .tab_5 .box label').click(function () {
            $('.tab_5 input').prop('checked', false)
            $(this).closest('.box').find('input').prop('checked', true)
            $('.tab_5 .dot').removeClass('checked')
            $('.tab_5 .dot .icon').addClass('hide')

            $('.moods .tab_5 .box').removeClass('active')
            $(this).closest('.box').addClass('active')
            $(this).find('.dot').addClass('checked')
            $(this).find('.dot .icon').removeClass('hide')
        })

    } else if($('body').hasClass('cadastro')) {

        //Switch Cpf|Cpnj
        $('.form_cadastro .switch li').click(function () {

            if($(this).attr('class') == 'cnpj'){
               $('.form_cadastro .full.cnpj').removeClass('hide')
               $('.form_cadastro .full.cpf').addClass('hide')
            }else{
                $('.form_cadastro .full.cpf').removeClass('hide')
                $('.form_cadastro .full.cnpj').addClass('hide')
            }
            $('.form_cadastro .switch li .dot').removeClass('checked')
            $('.form_cadastro .switch li .icon').addClass('hide')

            $(this).find('.dot').addClass('checked')
            $(this).find('.icon').removeClass('hide')
        })

    }
     else if($('body').hasClass('minha_conta')) {

        //Switch minha conta
        $('.minha_conta .nav_tab .item').click(function () {
            $('.minha_conta .nav_tab .item').removeClass('active')
            $(this).addClass('active')

            if($(this).hasClass('requests')){
                $('.minha_conta .tab.config').addClass('hide')
                $('.minha_conta .tab.requests').removeClass('hide')
            }else{
                $('.minha_conta .tab.config').removeClass('hide')
                $('.minha_conta .tab.requests').addClass('hide')
            }
        })
    }
    else if($('body').hasClass('editar_conta')) {
        //Switch Editar conta
        $('.editar_conta .nav_tab .item').click(function () {
            $('.editar_conta .nav_tab .item').removeClass('active')
            $(this).addClass('active')

            if($(this).hasClass('requests')){
                $('.editar_conta .tab.config').addClass('hide')
                $('.editar_conta .tab.requests').removeClass('hide')
            }else{
                $('.editar_conta .tab.config').removeClass('hide')
                $('.editar_conta .tab.requests').addClass('hide')
            }
        })
    }
    else {}
    // End funções em páginas específicas


    function show_mask() {
        $('.mask').removeClass('hide');
    }
    function hide_mask() {
        $('.mask').addClass('hide');
    }
    function hide_drop() {
        $('body').find('.drop_content').addClass('hide');
        //hide_mask();
    }
    function mark_off(){
	    $('.dot').removeClass('checked');
    }
    function anchor(elemento) {
        $('html, body').animate({
            scrollTop: $(elemento).offset().top
        }, 2000);
    }


    // Slider Range
    if($('body').hasClass('categoria')) { // Página Inicial
        var slider = document.getElementById("range_prod");
        var output = document.getElementById("value");

        output.innerHTML = slider.value;
        slider.oninput = function() {
            output.innerHTML = this.value;
        }
    }

    // Nav Filter and Order
    $('.btn_filter').click(function () {
        show_mask()
        $('.nav_drop').show()
        $('.nav_drop').removeClass('hide')
        $('.close_navdrop').removeClass('hide')
    })
    $('.btn_order').click(function () {
        show_mask()
        $('.nav_drop').show()
        $('.nav_drop').removeClass('hide')
        $('.close_navdrop').removeClass('hide')
        $('.dropdown').addClass('hide')
        $('.dropdown.drop_4').show()
        $('.dropdown.drop_4').removeClass('hide')
        $('.drop_4 .btn_drop').remove()
        $('.drop_content').removeClass('hide')

    })
    $('.close_navdrop').click(function () {
        $(this).closest('.nav_drop').addClass('hide')
        $('.dropdown').removeClass('hide')
        $('.drop_content').addClass('hide')

        hide_mask()
    })
    // :Nav Filter and Order

    // Navbar Sidebar
    $('.navbar .dropdown').click(function () {
        hide_drop();
        $(this).find('.drop_content').removeClass('hide');
        show_mask()
    });

    //Nav Drop Categoria
    $('.nav_drop .dropdown').click(function () {
        hide_drop();



        if($(this).find('.btn_drop').hasClass('active')){
            $(this).find('.btn_drop').removeClass('active');
            $(this).find('.drop.content').addClass('hide');
            return false;

        }

        $('.btn_drop').removeClass('active');
        $(this).find('.btn_drop').addClass('active');
        $(this).find('.drop_content').removeClass('hide');
    });

    $('.order .item').click(function () {
        mark_off();
        $(this).find('.dot').addClass('checked')
    })







});