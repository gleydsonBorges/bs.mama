	<header>
		<article>
            <!-- Logo:  -->
            <a href="<? echo $url ?>" id="logo_header" class="logo_header" title="<? echo $HomeAltTitle ?>" alt="<? echo $HomeAltTitle ?>">
                <img src="<? echo $images ?>logo.jpg" class="svg"/>
            </a>
            <!-- Logo:  -->
            <!-- Abrir menu Mobile: -->
            <a href="javascript:void(0)" class="mobile_item" id="AbrirMenu">
                <span id="Cima"></span>
                <span id="Meio"></span>
                <span id="Baixo"></span>
            </a>
            <!-- :Abrir menu Mobile -->

            <div class="client_area">
                <div id="search" class="search">
                    <a href="javascript:void(0)" class="click_search">
                        <img src="<? echo $url?>assets/images/icon/search.svg" id="btn_search" class="icon_search" alt="Campo de pesquisa"/>
                    </a>
                    <input type="text" id="txt_search" class="txt_search" placeholder="Pesquisar..."/>
                </div>
                <div class="login">
                    <a href="./minha-conta.php">
                        <img class="icon_login" src="<? echo $url?>assets/images/icon/profile.svg" alt="">
                    </a>
                </div>
                <div class="">
                    <a href="./carrinho.php" class="cart">
                        <img class="icon_cart" src="<? echo $url?>assets/images/icon/cart.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="toggler mobile_item"><img src="<? echo $url?>assets/images/icon/menu.svg" alt=""></div>
            <div class="close_navbar mobile_item hide"><img src="<? echo $url?>assets/images/icon/close.svg" alt=""></div>
            <div class="mask hide"></div>
            <div class="navbar">
                <div class="mobile_item">
                    <div class="client_area">
                        <img class="logo" src="<? echo $images ?>logo.jpg" class="svg"/>
                        <a class="icon" href="javascript:void(0)">
                            <img class="" src="<? echo $url?>assets/images/icon/search.svg" alt="">
                        </a>
                        <a class="icon" href="javascript:void(0)">
                            <img class="" src="<? echo $url?>assets/images/icon/profile.svg" alt="">
                        </a>
                        <a class="icon" href="javascript:void(0)">
                            <img class="" src="<? echo $url?>assets/images/icon/cart.svg" alt="">
                        </a>
                    </div>
                </div>
                <div class="item dropdown produtos drop_1">
                    <div class="btn_drop"><a class="link" href="javascript:void(0)">Produtos</a>
                        <img class="icon_down" src="" alt="">
                        <img class="icon_right" src="<? echo $url?>assets/images/icon/arrowright.svg" alt="">
                    </div>
                    <div class="drop_content hide">
                        <img class="mobile_item icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                        <span class="mobile_item subtitle">Produtos</span>
                        <article>
                            <ul class="list">
                                <li class="item"><a href="./categoria.php"> Adesivos  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></a></li>
                                <li class="item"><a href="./categoria.php">Plaquinhas decorativas  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Almofadas  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Posters  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Bordados  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./quadrinhos.php">Quadrinhos  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Painéis  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Réguas de crescimento  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./categoria.php">Papel de parede adesivo  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                            </ul>
                            <img class="img_drop" src="<? echo $url?>assets/images/img_drop.jpg" alt="">
                        </article>
                    </div>
                </div>
                <div class="item dropdown temas drop_2">
                    <div class="btn_drop "><a class="link" href="javascript:void(0)">Temas</a>
                        <img class="icon_down" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt="">
                        <img class="icon_right" src="<? echo $url?>assets/images/icon/arrowright.svg" alt="">
                    </div>
                    <div class="drop_content hide">
                        <img class="mobile_item icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                        <span class="mobile_item subtitle">Temas</span>
                        <article>
                            <ul class="list">
                                <li class="item"><a href="./temas.php">Mininamista 1  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./temas.php">Preto e branco  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./temas.php">Coloridas  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./temas.php">Basic  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./temas.php">Bichinhos  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                                <li class="item"><a href="./temas.php">Estampados  <img src="<? echo $url?>assets/images/balloon.png" class="icon" alt=""></li></a>
                            </ul>
                            <img class="img_drop" src="<? echo $url?>assets/images/img_drop.jpg" alt="">
                        </article>
                    </div>
                </div>
                <a class="item shop_the_look link" href="./shop-the-look.php">Shop the look</a>
                <a class="item moods link" href="./projeto.php">Projeto de decoração</a>
                <a class="item" href="./categoria.php">Sale</a>
                <img class="img bar" src="<? echo $url?>assets/images/icon/bar.svg" alt="">
                <a class="item inspiracoes link" href="./inspiracoes.php">Inspirações</a>
            </div>
        </article>
	</header>