<!--
		Hello, guys!! :)

		Todo o site foi desenvolvido pela bs.agency em 2020. 
		Façam bom uso!
-->
<?
	$empresa = 'Mama Loves You - Estúdio de Decoração Infantil';
	$CNPJ = '26.183.213/0001-90';
	//$url = 'http://previewbsagency.com.br/mama/';
	$url = 'http://mama.ko/';
	$images = $url.'assets/images/';
	$Tags = '';
	
	// Páginas
		$Pagina = $url.'pagina.php';
		$Sobre = $url.'sobre.php';
		$Contato = $url.'contato.php';

	// Alt Title 
		$CompartilhamentoAltTitle = '';
		$HomeAltTitle = '';
		$SobreAltTitle = '';
		$ContatoAltTitle = '';

	// contato
		$email = 'pedepara@mamalovesyou.com.br';

	// Social
		$facebookPage = 'mamalovesyoubrasil';
		$facebookid = '1101227723318136';
		$instagram = '_mamalovesyou';

	// Telefones
		$teltxt = '(11) 3722-4500';
		$tellink = 'tel:+551137224500';

	// Whatsapp 
		$whatsapptxt = '(11) 98098-8840';
		$whatsapplink =  'https://api.whatsapp.com/send?phone=5511980988840&text=Estou%20entrando%20em%20contato%20através%20do%20website';

	// Location
		$waze = 'waze://?q='. $empresa;
		$mapslocation = 'http://maps.google.com/?q='. $empresa;
		$GoogleMapsKey = '';
?>