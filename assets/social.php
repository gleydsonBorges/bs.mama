<div class="social_links top32">
	<!-- Facebook Desktop -->
	<a href="https://www.facebook.com/<? echo $facebookPage?>/" class="desktop_item" target="_blank">
        <img class="icon" src="<? echo $url?>assets/images/social/facebook.svg" alt="">
	</a>
	<!-- Facebook iOS -->
	<a href="fb://page/?id=<? echo $facebookid?>" class="mobile_item iOS">
        <img class="icon" src="<? echo $url?>assets/images/social/facebook.svg" alt="">
	</a>
	<!-- Facebook Android -->
	<a href="fb://page/<? echo $facebookid?>" class="mobile_item Android">
        <img class="icon" src="<? echo $url?>assets/images/social/facebook.svg" alt="">
	</a>
	<!-- Instagram -->
	<a href="<? echo $instagram ?>" target="_blank">
        <img class="icon" src="<? echo $url?>assets/images/social/insta.svg" alt="">
	</a>
	<!-- Pinterest -->
	<!-- <a href="<? echo $pinterest ?>" target="_blank">
		<i class="fab fa-pinterest-p"></i>
	</a> -->
	<!-- Linkedin -->
	<!-- <a href="<? echo $linkedin ?>" target="_blank">
		<i class="fab fa-linkedin"></i>
	</a> -->
</div>