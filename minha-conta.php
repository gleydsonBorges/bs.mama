<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="minha_conta">
    <?php include 'assets/header.php'; ?>
    <section>
        <article class="main">
            <h2 class="main_title center">Minha conta</h2>
            <div class="nav_tab">
                <span class="item config active">Configurações</span>
                <span class="item requests">Meus Pedidos</span>
            </div>
            <div class="tab config">
                <div class="information_account">
                    <h3 class="title">Informações da conta</h3>
                    <div class="item">
                        <p class="name">E-mail</p>
                        <p class="info">email@email.com.br</p>
                    </div>
                    <div class="item">
                        <p class="name">senha</p>
                        <a href="#" class="info">Trocar senha</a>
                    </div>
                </div>
                <div class="personal_data">
                    <h3 class="title">Dados Pessoais</h3>
                    <div class="item w350">
                        <p class="name">Nome completo</p>
                        <p class="info">Fulano Ciclano da silva</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Data de Nascimento</p>
                        <p class="info">12/12/2012</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Gênero</p>
                        <p class="info">Masculino</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Telefone</p>
                        <p class="info">(11) 9999-9999</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Celular</p>
                        <p class="info">(11) 99999-9999</p>
                    </div>
                    <div class="item w350">
                        <p class="name">CPF</p>
                        <p class="info">999.999.999-99</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Possui Filhos?</p>
                        <p class="info">Sim</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Faixa etária</p>
                        <p class="info">2-4 anos</p>
                    </div>
                </div>
                <div class="address">
                    <h3 class="title">Endereço</h3>
                    <div class="item full">
                        <p class="name">CEP</p>
                        <p class="info">999999-99</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Endereço</p>
                        <p class="info">Rua 12 de dezembro</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Número</p>
                        <p class="info">99</p>
                    </div>
                    <div class="item w160">
                        <p class="name">Complemento</p>
                        <p class="info">Apto 99</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Bairro</p>
                        <p class="info">Vila Bairro</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Referência</p>
                        <p class="info">Ao lado do local</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Cidade</p>
                        <p class="info">São José da cidade</p>
                    </div>
                    <div class="item w350">
                        <p class="name">Estado</p>
                        <p class="info">São Paulo</p>
                    </div>
                </div>
                <a href="./editar" class="btn upp center top64">Editar</a>
            </div>
            <div class="tab requests hide">
                <div class="list">
                    <p class= "message hide">Voce ainda não fez nenhum pedido com a gente</p>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                </div>
            </div>
        </article>
    </section>



    <?php include 'assets/footer.php'; ?>
</body>
</html>