<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="carrinho">
    <?php include 'assets/header.php'; ?>
    <section>
        <h2 class="main_title center">Carrinho</h2>
        <article class="main">
            <div class="grid_prod">
                <div class="item">
                    <div class="sub">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <h4 class="title">Nome do produto em até duas linhas</h4>
                        <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                        <p class="frame"><span>Moldura:</span> Sem moldura</p>
                        <p class="color"><span>Cor:</span> Azul Royal</p>
                    </div>
                    <div class="sub">
                        <p class="center question">Quantidade:</p>
                        <div class="more_less center">
                            <img class="icon minus" src="<? echo $url?>assets/images/icon/minus.svg" alt="">
                            <p><span class="result">1 </span> Quadro</p>
                            <img class="icon plus" src="<? echo $url?>assets/images/icon/plus.svg" alt="">
                        </div>
                        <div class="divider"></div>
                        <p class="value center">RS YY,00</p>
                        <p class="center">(R$ XX,00/unidade)</p>
                        <div class="divider"></div>
                        <div class="btn_remove">Remover item <img class="icon" src="<? echo $url?>assets/images/icon/close.svg" alt=""></span></div>
                    </div>
                 </div>
                <div class="item">
                    <div class="sub">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <h4 class="title">Nome do produto em até duas linhas</h4>
                        <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                        <p class="frame"><span>Moldura:</span> Sem moldura</p>
                        <p class="color"><span>Cor:</span> Azul Royal</p>
                    </div>
                    <div class="sub">
                        <p class="center question">Quantidade:</p>
                        <div class="more_less center">
                            <img class="icon minus" src="<? echo $url?>assets/images/icon/minus.svg" alt="">
                            <p><span class="result">1 </span> Quadro</p>
                            <img class="icon plus" src="<? echo $url?>assets/images/icon/plus.svg" alt="">
                        </div>
                        <div class="divider"></div>
                        <p class="value center">RS YY,00</p>
                        <p class="center">(R$ XX,00/unidade)</p>
                        <div class="divider"></div>
                        <div class="btn_remove">Remover item <img class="icon" src="<? echo $url?>assets/images/icon/close.svg" alt=""></span></div>
                    </div>
                </div>
                <div class="item">
                    <div class="sub">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <h4 class="title">Nome do produto em até duas linhas</h4>
                        <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                        <p class="frame"><span>Moldura:</span> Sem moldura</p>
                        <p class="color"><span>Cor:</span> Azul Royal</p>
                    </div>
                    <div class="sub">
                        <p class="center question">Quantidade:</p>
                        <div class="more_less center">
                            <img class="icon minus" src="<? echo $url?>assets/images/icon/minus.svg" alt="">
                            <p><span class="result">1 </span> Quadro</p>
                            <img class="icon plus" src="<? echo $url?>assets/images/icon/plus.svg" alt="">
                        </div>
                        <div class="divider"></div>
                        <p class="value center">RS YY,00</p>
                        <p class="center">(R$ XX,00/unidade)</p>
                        <div class="divider"></div>
                        <div class="btn_remove">Remover item <img class="icon" src="<? echo $url?>assets/images/icon/close.svg" alt=""></span></div>
                    </div>
                </div>
                <div class="item">
                    <div class="sub">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <h4 class="title">Nome do produto em até duas linhas</h4>
                        <p class="size"><span>Tamanho:</span> A4 (21 x 29,7cm) </p>
                        <p class="frame"><span>Moldura:</span> Sem moldura</p>
                        <p class="color"><span>Cor:</span> Azul Royal</p>
                    </div>
                    <div class="sub">
                        <p class="center question">Quantidade:</p>
                        <div class="more_less center">
                            <img class="icon minus" src="<? echo $url?>assets/images/icon/minus.svg" alt="">
                            <p><span class="result">1 </span> Quadro</p>
                            <img class="icon plus" src="<? echo $url?>assets/images/icon/plus.svg" alt="">
                        </div>
                        <div class="divider"></div>
                        <p class="value center">RS YY,00</p>
                        <p class="center">(R$ XX,00/unidade)</p>
                        <div class="divider"></div>
                        <div class="btn_remove">Remover item <img class="icon" src="<? echo $url?>assets/images/icon/close.svg" alt=""></span></div>
                    </div>
                </div>
            </div>
            <div class="info">
                <div class="item subtotal">
                    <p class="center">Subtotal</p>
                    <p class="value center">RS YY,00</p>
                </div>
                <div class="item calc_frete">
                    <p>Frete</p>
                    <input type="text" placeholder="Endereço">
                    <select name="" id="" >
                        <option value="Tipo de frete">Tipo de frete</option>
                        <option value="">Opção</option>
                        <option value="">Opção</option>
                        <option value="">Opção</option>
                        <option value="">Opção</option>
                        <option value="">Opção</option>
                    </select>
                    <p class="value center">RS YY,00</p>
                </div>
                <div class="item desconto">
                    <p>Desconto</p>
                    <input type="text" placeholder="">
                    <p class="value center">- RS YY,00</p>
                </div>
                <div class="divider"></div>
                <div class="item total">
                    <p>Total</p>
                    <p class="value center">RS YY,00</p>
                </div>
                <a href="/" class="btn btn_next upp center top32">Avançar</a>

            </div>
        </article>
    </section>

    <?php include 'assets/footer.php'; ?>
</body>
</html>