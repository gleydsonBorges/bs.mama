<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="carrinho ready">
    <?php include 'assets/header.php'; ?>
    <article class="status">
        <p>Pronto!</p>
        <p>Agora é só acompanhar o pedido pelo seu e-mail!</p>
        <a href="/" class="btn upp center top64">Voltar a navegação</a>
    </article>
    <section class="insta">
        <article>
            <div class="header">
                <h6 class="title">Siga a Mama no Insta!</h6>
            </div>
        </article>
        <div class="gallery">
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_3.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_4.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_5.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
        </div>
        <p class="center account">@_mamalovesyou</p>
    </section>
    <?php include 'assets/footer.php'; ?>
</body>
</html>