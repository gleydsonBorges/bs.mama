<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="sobre">
    <?php include 'assets/header.php'; ?>
    <section class="slider_sobre">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section>
        <article class="main">
            <h2 class="main_title center">Sobre nós</h2>
            <div class="box">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="text">
                    Na Mama Loves You, acreditamos no poder da estamparia como forma de expressão para criar um mundo onde é mais divertido viver. Amamos cores e design, mais ainda quando são feitos com diversão, e é isso que nós fazemos: “coisas” que fazem paredes transmitir amor e alegria, gerando ambientes onde famílias tenham deliciosas histórias para contar. Se pudermos fazer parte dessas histórias, pronto: nossa missão estará cumprida.
                </p>
                <p>A grande inspiração de nossas estampas é o amor, seja para embalar o mundo maravilhoso em que vivem as crianças ou para trazer mais leveza ao mundo dos adultos. E é isso que queremos levar a você, na forma de papéis de parede, murais e adesivos pensados para encher sua casa de cor, vida e alegria!</p>
            </div>
        </article>
    </section>
    <section class="who">
        <article class="main">
            <h1 class="sub_title">Quem faz a Mama Loves You</h1>
            <div class="box">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="text">
                    A Mama Loves You tem pai e mãe, Sergio Spínola e Mariana Liporoni, são um casal que fazem tudo junto e misturado. Eles se conheceram no mundo corporativo do varejo de moda, e a sinergia rolou desde então. Depois de anos de dedicação em grandes empresas do setor, passando por Pernambucanas, C&A e Riachuelo, foi em 2016 que resolveram deixar de lado suas carreiras bem sucedidas, para investir no sonho de empreender. Depois de muitos planos e ideias nasceu a Mama Loves You, um studio de “coisas” para parede que juntou o amor pela decoração e pela arte com o amor pelo universo infantil, que veio a partir do momento que juntaram as famílias.
                </p>
                <p>Hoje a família Mama Loves You é formada pelo Sergio e pela Mari, e também pelo João e pelo Neto, buldogue que tá mais pra filho do que pra cachorro!</p>
            </div>
        </article>
    </section>


    <section class="insta">
        <article>
            <div class="header">
                <h6 class="title">Siga a Mama no Insta!</h6>
            </div>
        </article>
        <div class="gallery">
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_3.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_4.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_5.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
        </div>
        <p class="center account">@_mamalovesyou</p>
    </section>

    <?php include 'assets/footer.php'; ?>
</body>
</html>