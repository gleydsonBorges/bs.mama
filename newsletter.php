<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="newsletter">
<?php include 'assets/header.php'; ?>
<section>
    <article class="container">
        <div class="row">
            <div class="col">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </div>
            <div class="col">
                <img class="btn_close" src="<? echo $url?>assets/images/icon/close.svg" alt="">
                <h1 class="title">Quer receber 5% de desconto na sua compra?</h1>
                <p class="text">Inscreva-se na Newsletter da Mama e receba novidades, promoções e descontos! </p>
                <form action="" method="get">
                    <input type="email" placeholder="E-mail" class="email">
                    <input type="submit" class="btn upp center" value="inscrever">
                </form>
            </div>
        </div>


    </article>
</section>

<?php include 'assets/footer.php'; ?>
</body>
</html>