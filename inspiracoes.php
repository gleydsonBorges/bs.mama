<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="inspiracoes">
    <?php include 'assets/header.php'; ?>
    <section class="slider_inspiracoes">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section class="produtos">
        <article class="center">
            <h1 class="main_title center">Inspirações e projetos realizados</h1>
            <p class="text">Está procurando ideias para o seu ambiente?</p>
            <p class="text">Confira fotos de projetos de quem já usou os produtos da Mama!</p>
            <button class="btn_filter mobile_item">Filtrar<img class="icon" src="<? echo $url?>assets/images/icon/filter.svg" alt=""></button>
            <button class="btn_order mobile_item">Ordenar<img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
            <div class="nav_drop">
                <div class="close_navdrop mobile_item hide"><img src="http://mama.ko/assets/images/icon/close.svg" alt=""></div>
                <div class="dropdown drop_1">
                    <button class="btn_drop"><span>Gênero</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list">
                            <li class="item">Banner<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="item">Minimalista<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="divider"></li>
                            <li class="item">Preto e Branco<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item">Coloridos<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item">Basic<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item">Bichinhos<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item">Estampados<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown drop_2">
                    <button class="btn_drop"><span>Idade</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list">
                            <li class="item"><span class="figure"></span>21cm x 21cm<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="divider"></li>
                            <li class="item"><span class="figure a4"></span>A4 (21cm x 29,7cm)<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="figure a3"></span>A3 (29,7cm x 42cm)<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="figure a2"></span>A2 (42cm x 59,4cm)<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown drop_3 hide">
                    <button class="btn_drop"><span>Tamanho</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list">
                            <li class="item"><span class="figure"></span>Pequeno<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="divider"></li>
                            <li class="item"><span class="figure a4"></span>Pequeno<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="figure a3"></span>Médio <img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="figure a2"></span>Grande <img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown drop_4 hide">
                    <button class="btn_drop"><span>Cor</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list color">
                            <li class="item"><span class="dot" style="background-color: "></span>Amarelo<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Azul<img class="icon" src="<? echo $url?>assets/images/icon/close.svg" class="icon" alt=""></li>
                            <li class="divider"></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Bege<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Branco<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Cinza<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Multicor<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Preto<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Roxo<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                            <li class="item"><span class="dot" style="background-color: "></span>Verde<img class="icon" src="<? echo $url?>assets/images/icon/plus.svg" class="icon" alt=""></li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown drop_5 hide">
                    <button class="btn_drop"><span>Preço</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list">
                            <li class="item"><input type="range" min="0" max="1000" value="400" step="10" class="slider" id="range_prod"></li>
                            <li class="item"><p class="price">Até: R$ <span id="value"></span>,00</p></li>
                        </ul>
                    </div>
                </div>
                <img class="img bar hide" src="<? echo $url?>assets/images/icon/bar.svg" alt="">
                <div class="dropdown drop_6 hide">
                    <button class="btn_drop"><span>Ordenar </span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list order">
                            <li class="item"><span class="dot checked"></span>Ordem alfabética</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Mais vendidos</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Menos preço</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Maior preço</li>
                        </ul>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section>
        <article class="grid products">
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>
            <a class="item" href="./inspiracoes-open.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="title">Papel de parede adesivo</p>
            </a>


        </article>
    </section>
    <div class="pagination mobile_item">
        <div class="center">
            <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowleft" alt=""></a>
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowright" alt=""></a>
        </div>
    </div>

    <?php include 'assets/footer.php'; ?>
</body>
</html>