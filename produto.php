<!DOCTYPE HTML>
<html>
	<head>
	<?php 
		include 'assets/config.php';
 
		$tituloPagina = ''. $empresa;
		$descricaoCompartilhamento = '';
		
		$urlPagina = $url;
		$imagemCompartilhamento = $images.'anuncio_demanda_08.png';
		$sessao = 'home';
	?>
    <title><? echo $tituloPagina ?></title>
	<?php include 'assets/head.php'; ?>

		<meta property="og:title" content="<? echo $tituloPagina ?>" />
		<meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
		<meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

	</head>
	<body class="produto" >
    <?php include 'assets/header.php'; ?>

    <section>
        <article>
            <div class="selected_category">
                <a class="link" href=".\categoria.php" >
                    <img class="icon icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                    Papel de parede adesivo
                </a>
            </div>
            <div class="selected_product">
                <div class="gallery">
                    <div class="services-slider">
                        <div class="main-container">
                            <div class="slider slider-main">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="nav-container">
                            <div class="slider-nav">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="options">
                    <div class="divider mobile_item"></div>
                    <h3 class="title" id="title_produto">Nome do produto ocupando até três linhas</h3>
                    <p class="center question">Qual é o tamanho da sua parede?</p>
                    <div id="calc" class="calc">
                        <div class="item">
                            <label for="">Largura (cm)</label>
                            <input type="text"> <br>
                        </div>
                        <div class="item">
                            <label for="">Altura (cm)</label>
                            <input type="text">
                        </div>
                        <div>
                            <button class="btn center upp"> Calcular</button>
                        </div>
                    </div>
                    <div class="divider top32 bot32"></div>
                    <p class="center">Para sua parede de Z,Z m² você vai precisar de: </p>
                    <p class="result center">3 rolos de W,W m² </p>
                    <div class="colors center">
                        <p class="center">Na cor:</p>
                        <span class="dot checked" style="background-color:#0CABE9 "></span>
                        <span class="dot" style="background-color:#B4A259 "></span>
                        <span class="dot" style="background-color:#000000 "></span>
                        <span class="dot" style="background-color:#D712E5 "></span>
                        <span class="dot" style="background-color:#079C39 "></span>
                        <span class="dot" style="background-color:#78079C "></span>
                    </div>
                    <div class="divider top32 bot32"></div>
                    <p class="value center">RS YY,00</p>
                    <p class="installments center bot32">em até 3x de R$ XX,00 sem juros
                        ou R$ XX,00 no boleto bancário
                    </p>
                    <button class="btn center upp">Adicionar ao carrinho</button>

                </div>
            </div>
        </article>
    </section>
    <section class="about_product">
        <article style="background-image:url('assets/images/produto.jpg');background-size: cover">
            <div class="desctiption">
                <h3 class="title">Título sobre o produto</h3>
                <p class="text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elit orci, feugiat ut cursus a, auctor ac sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean malesuada, sem sed ullamcorper dapibus, ligula risus convallis eros, vel varius libero mauris eget est. Etiam ut elit fringilla orci ornare laoreet. Proin elementum sem ut vehicula pretium.
                </p>
            </div>
            <div class="specifications">
                <p class="top32">Material</p>
                <p>Vinil adesivo</p>
                <p>Acabamento</p>
                <p>Recortado sem relevo fosco</p>
                <p>Peso</p>
                <p>500g/m²</p>
                <p>Tamanho</p>
                <p>0,33m x 0,6m</p>
                <p>Produção</p>
                <p>Em até 5 dias úteis</p>
            </div>
        </article>
    </section>
    <section class="related_products">
        <article><h3 class="title">Produtos Relacionados</h3></article>
        <article class="grid products">
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
           <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
           <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
           <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Papel de parede adesivo</p>
                <p class="product">Monstros de Colorir</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <a href="/categoria" class="more">ver mais ></a>
        </div>
    </section>
    <section>
        <article class="custom">
            <h5 class="title">Faça seu produto personalizado!</h5>
            <p class="center">Não encontrou a estampa, cor ou tamanho que queria? <br>
                Manda uma mensagem pra gente, vamos adorar criar <br> um produto especial só pra você!
            </p>
            <form action="#" class="form top32">
                <label for="name">Nome</label>
                <input type="text" id="name" name="firstname" placeholder="">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" placeholder="">
                <label for="subject">Mensagem</label>
                <textarea id="subject" name="subject" placeholder="" style="height:200px"></textarea>
                <input class="btn upp center top32" type="submit" value="mandar mensagem">
            </form>
        </article>
    </section>
    <section class="insta">
        <article>
            <div class="header">
                <h6 class="title">Siga a Mama no Insta!</h6>
            </div>
        </article>
        <div class="gallery">
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_3.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_4.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_5.png" alt=""></a>
            <a href="#" class="item"><img src="<? echo $url?>assets/images/insta_2.png" alt=""></a>
        </div>
        <p class="center account">@_mamalovesyou</p>
    </section>





    <?php include 'assets/footer.php'; ?>
	</body>
</html>