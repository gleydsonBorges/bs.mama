<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="editar_conta">
    <?php include 'assets/header.php'; ?>
    <section>
        <article class="main">
            <h2 class="main_title center">Editar conta</h2>
            <div class="nav_tab">
                <span class="item config active">Configurações</span>
                <span class="item requests">Meus Pedidos</span>
            </div>
            <div class="tab config">
                <form action="" method="get" class="form_edit">
                    <div class="information_account">
                        <h3 class="sub_title">Informações da conta</h3>
                        <div class="w350 r30">
                            <label for="email">E-mail</label>
                            <input type="email" id="email" name="email" placeholder="" required="" />
                        </div>
                        <div class="w350">
                            <label for="check_email">Confirmar e-mail</label>
                            <input type="email" id="check_email" name="check_email" placeholder="" required="" />
                        </div>
                    </div>
                    <div class="personal_data">
                        <h3 class="sub_title">Dados pessoais</h3>
                        <div class="w350 top32 r30">
                            <label for="name">Nome Completo*</label>
                            <input type="text" id="name" name="name" placeholder="" required="" />
                        </div>
                        <div class="full_m">
                            <div class="w160 top32 r30 dt_of_birth">
                                <label for="dt_of_birth">Data de Nascimento*</label>
                                <select name="dt_of_birth" id="dt_of_birth">
                                    <option value=""></option>
                                    <option value="Opcao">Opção 1</option>
                                    <option value="Opcao">Opção 2</option>
                                    <option value="Opcao">Opção 3</option>
                                    <option value="Opcao">Opção 4</option>
                                </select>
                            </div>
                            <div class="w160 top32 genre">
                                <label for="genre">Gênero*</label>
                                <select name="genre" id="genre">
                                    <option value=""></option>
                                    <option value="Opcao">Opção 1</option>
                                    <option value="Opcao">Opção 2</option>
                                    <option value="Opcao">Opção 3</option>
                                    <option value="Opcao">Opção 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="w350 r30">
                            <label for="phone">Telefone</label>
                            <input type="text" id="phone" name="phone" placeholder="" required="" />
                        </div>
                        <div class="w350">
                            <label for="cell">Celular</label>
                            <input type="text" id="cell" name="cell" placeholder="" required="" />
                        </div>
                        <div class="full_m cpf">
                            <div class="w350 r30">
                                <label for="cpf">CPF*</label>
                                <input type="text" id="cpf" name="cpf" placeholder="" required="" />
                            </div>
                            <div class="w160 r30">
                                <label for="number_of_children">Pussui filhos?</label>
                                <select name="number_of_children" id="number_of_children">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="w160">
                                <label for="age" class="age">Faixa etária</label>
                                <select name="age" id="age" disabled>
                                    <option value=""></option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="full cnpj hide">
                            <div class="w350 r30">
                                <label for="cnpj">CNPJ*</label>
                                <input type="text" id="cnpj" name="cnpj" placeholder="" required="" />
                            </div>
                            <div class="w350">
                                <label for="registration">Inscrição Estadual</label>
                                <input type="text" id="registration" name="registration" placeholder="" required="" />
                            </div>
                        </div>
                    </div>
                    <div class="personal_address">
                        <h3 class="sub_title">Endereço</h3>
                        <div class="full r30 cep">
                            <div class="w160">
                                <label for="cep">CEP</label>
                                <input type="text" id="cep" name="cnpj" placeholder="" required="" />
                            </div>
                            <div class="w160">
                                <input class="btn upp center btn_send" type="submit" value="Buscar por CEP">
                            </div>
                        </div>
                        <div class="w350 r30">
                            <label for="address">Endereço*</label>
                            <input type="text" id="address" name="address" placeholder="" required="" />
                        </div>
                        <div class="full_m">
                            <div class="w160 r30">
                                <label for="number">Número*</label>
                                <input type="text" id="number" name="number" placeholder="" required="" />
                            </div>
                            <div class="w160">
                                <label for="complement">Complemento*</label>
                                <select name="complement" id="complement">
                                    <option value=""></option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="w350 r30">
                            <label for="district">Bairro*</label>
                            <input type="text" id="district" name="district" placeholder="" required="" />
                        </div>
                        <div class="w350">
                            <label for="ref">Referência</label>
                            <input type="text" id="ref" name="ref" placeholder="" required="" />
                        </div>
                        <div class="w350 r30">
                            <label for="city">Cidade</label>
                            <input type="text" id="city" name="city" placeholder="" required="" />
                        </div>
                        <div class="full_m">
                            <div class="w160 state">
                                <label for="state">Estado*</label>
                                <select name="state" id="state">
                                    <option value=""></option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <input class="btn upp center top64" type="submit" value="Salvar">
                </form>


            </div>
            <div class="tab requests hide">
                <div class="list">
                    <p class= "message hide">Voce ainda não fez nenhum pedido com a gente</p>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                    <div class="item">
                        <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                        <p><span>Número do pedido:</span> 99999</p>
                        <p><span>Pedido realizado dia:</span> 12/12/2012</p>
                        <p><span>Endereço de entrega:</span> Rua 12 de dezembro, 99, Vila Bairro, São José da Cidade - SP, 99999-99 </p>
                        <p><span>Valor total:</span> R$ 99,00</p>
                        <p><span>Status:</span> Entregue</p>
                        <a href="./pedido-open.php" class="btn_view">Ver pedido completo</a>
                    </div>
                </div>
            </div>
        </article>
    </section>



    <?php include 'assets/footer.php'; ?>
</body>
</html>