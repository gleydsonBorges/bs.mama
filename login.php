<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="login">
    <?php include 'assets/header.php'; ?>
    <section class="">
        <article class="center">
            <h1 class="main_title center">Bem-vindo</h1>
        </article>
    </section>
    <section class="container">
        <article class="row">
            <div class="col w350">
                <p>Já tenho cadastro</p>
                <form action="" method="get" class="form_login">
                    <label for="">E-mail</label>
                    <input type="e-mail">
                    <label for="">Senha</label>
                    <input type="password">
                    <a href="" class="btn_forgot">Esqueci minha senha</a>
                    
                    <input class="btn upp center btn_login top64" type="submit" value="fazer login">
                    <br>
                    <p class="bold">ou</p>
                    <a href="" class="btn upp center btn_facebook"><img class="ico" src="<?echo $url?>assets/images/social/facebook.ico" alt="">login com facebook</a>
                </form>
            </div>
            <div class="col">
                <p>Ainda não sou cadastrado</p>
                <input class="btn upp center btn_login top64" type="submit" value="cadastrar">
                <br>
                <p class="bold">ou</p>
                <a href="" class="btn upp center btn_facebook">
                    <img class="ico" src="<?echo $url?>assets/images/social/facebook.ico" alt="">Cadastrar com facebook</a>
            </div>
        </article>
    </section>

    <?php include 'assets/footer.php'; ?>
</body>
</html>