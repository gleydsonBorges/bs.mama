<!DOCTYPE HTML>
<html>
	<head>
	<?php 
		include 'assets/config.php';
 
		$tituloPagina = ''. $empresa;
		$descricaoCompartilhamento = '';
		
		$urlPagina = $url;
		$imagemCompartilhamento = $images.'anuncio_demanda_08.png';
		$sessao = 'home';
	?>
    <title><? echo $tituloPagina ?></title>
	<?php include 'assets/head.php'; ?>

		<meta property="og:title" content="<? echo $tituloPagina ?>" />
		<meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
		<meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

	</head>
	<body class=" inspiracoes_open" >
    <?php include 'assets/header.php'; ?>
    <section>
        <article>
            <div class="selected_category">
                <a class="link" href=".\inspiracoes.php" >
                    <img class="icon icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                    Inspirações
                </a>
            </div>
            <div class="selected_product selected_inspiration">
                <div class="gallery">
                    <div class="services-slider">
                        <div class="main-container">
                            <div class="slider slider-main">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="nav-container">
                            <div class="slider-nav">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="related_products">
        <article><h3 class="title">Produtos da Mama usados nesse look</h3></article>
        <article class="grid products">
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
            <div class="item">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="product">Nome do produto ocupando até duas linhas</p>
                <span class="price center">R$ XX,00</span>
                <a href="#" class="btn upp center">Adicionar ao carrinho</a>
            </div>
        </article>
        </div>
    </section>
    <?php include 'assets/footer.php'; ?>
	</body>
</html>