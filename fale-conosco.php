<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="fale_conosco">
    <?php include 'assets/header.php'; ?>
    <section class="slider_fale_conosco mobile_item">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section class="">
        <article class="center">
            <h1 class="main_title center">Fale Conosco</h1>
        </article>
    </section>
    <section class="container">
        <article class="row">
            <div class="col w350">
                <p>Problemas? Dúvidas? Sugestões? <br>
                    Manda uma mensagem pra Mama!
                </p>
                <form action="#" class="form form_faleConosco top32">
                    <div class="w350">
                        <label for="name">Nome</label>
                        <input type="text" id="name" name="firstname" placeholder="">
                        <label for="email">E-mail</label>
                        <input type="email" id="email" name="email" placeholder="">
                        <label for="subject">Mensagem</label>
                        <textarea id="subject" name="subject" placeholder="" class="w350" style="height:108px"></textarea>
                    </div>
                    <input class="btn upp center top32" type="submit" value="mandar mensagem">
                </form>
            </div>
            <div class="col">
                <p>Ou venha nos visitar na nossa lojinha!</p>

                <p class="address">MMLY ESTÚDIO DE DECORAÇÃO EIRELI <br>
                    Rua Regente Leon Kaniefsky, 450 <br>
                    Vila Progredior, São Paulo, SP 05617-030</p>

                <p><img class="icon" src="<? echo $url?>assets/images/icon/whatsapp.svg" alt="">(11) 3722-4500</p>
                <p><img class="icon" src="<? echo $url?>assets/images/icon/phone.svg" alt=""><a href="<? echo $whatsapplink ?>"><? echo $whatsapptxt ?></a></p>
                <p><img class="icon" src="<? echo $url?>assets/images/icon/email.svg" alt=""><a href="mailto:<? echo $email ?>"><? echo $email ?></a></p>
                <div class="social_links top32">
                    <!-- Facebook Desktop -->
                    <a href="https://www.facebook.com/<? echo $facebookPage?>/" class="desktop_item" target="_blank">
                        <img class="icon fb" src="<? echo $url?>assets/images/social/Facebook_Square_icon-icons.com_49948.ico" alt="">
                    </a>
                    <!-- Facebook iOS -->
                    <a href="fb://page/?id=<? echo $facebookid?>" class="mobile_item iOS">
                        <img class="icon fb" src="<? echo $url?>assets/images/social/Facebook_Square_icon-icons.com_49948.ico" alt="">
                    </a>
                    <!-- Facebook Android -->
                    <a href="fb://page/<? echo $facebookid?>" class="mobile_item Android">
                        <img class="icon fb" src="<? echo $url?>assets/images/social/Facebook_Square_icon-icons.com_49948.ico" alt="">
                    </a>
                    <!-- Instagram -->
                    <a href="<? echo $instagram ?>" target="_blank">
                        <img class="icon" src="<? echo $url?>assets/images/social/instagram-photo-share-social-media_icon-icons.com_61427.ico" alt="">
                    </a>
                    <!-- Pinterest -->
                    <!-- <a href="<? echo $pinterest ?>" target="_blank">
	                	<i class="fab fa-pinterest-p"></i>
	                </a> -->
                    <!-- Linkedin -->
                    <!-- <a href="<? echo $linkedin ?>" target="_blank">
		            <i class="fab fa-linkedin"></i>
	                </a> -->
                </div>
                
            </div>
        </article>
    </section>

    <?php include 'assets/footer.php'; ?>
</body>
</html>