<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="shop_the_look">
    <?php include 'assets/header.php'; ?>
    <section class="slider_shop_the_look">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section class="">
        <article class="center">
            <h1 class="main_title center">Shop the look</h1>
            <p class="text center">Confira looks completos feitos usando os produtos da Mama e leve um projeto de decoração pronto pro seu ambiente!</p>
        </article>
    </section>
    <section>
        <article class="grid products">
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>
            <a class="item" href="./shop-the-look-open.php">
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
            </a>


        </article>
        <div class="pagination">
            <div class="center">
                <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowleft" alt=""></a>
                <a href="#" class="active">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowright" alt=""></a>
            </div>
        </div>
    </section>
    <?php include 'assets/footer.php'; ?>
</body>
</html>