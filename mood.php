<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="moods">
    <?php include 'assets/header.php'; ?>
    <section class="slider_moods mobile_item">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section class="status" id="status">
       <div class="line"></div>
       <ul class="list">
           <li class="item step1">
               <span class="dot active"></span>
               <span class="name">Sobre Você</span>
           </li>
           <li class="item step2">
               <span class="dot"></span>
               <span class="name">Estilo</span>
           </li>
           <li class="item step3">
               <span class="dot"></span>
               <span class="name">Inspirações</span>
           </li>
           <li class="item step4">
               <span class="dot"></span>
               <span class="name">Referências</span>
           </li>
           <li class="item step5">
               <span class="dot"></span>
               <span class="name">Finalizar</span>
           </li>
       </ul>
    </section>
    <section class="tabs">
    <article>
            <form action="/mood-ready" method="GET" class="form form_mood">
                <div class="tab tab_1 " tab="1">
                   <h3 class="title center">Pra começar, conte um pouco sobre você e sobre o ambiente que você quer decorar</h3>
                   <h3 class="title center mobile_item">Veja como é fácil</h3>
                        <div class="col">
                            <label for="name">Nome*</label>
                            <input type="text" id="name" name="name" placeholder="">

                            <label for="email">E-mail*</label>
                            <input type="email" id="email" name="email" placeholder="">

                            <label for="phone">Telefone*</label>
                            <input type="text" id="phone" name="phone" placeholder="">
                        </div>
                        <div class="col">
                            <label for="dt_of_birth">Data de nascimento*</label>
                            <input type="text" id="dt_of_birth" name="dt_of_birth" placeholder="">

                            <label for="genre">Gênero*</label>
                            <select name="genre" id="genre">
                                <option value=""></option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino">Feminino</option>
                            </select>

                            <label for="type_of_room">Tipo de quarto*</label>
                            <select name="type_of_room" id="type_of_room">
                                <option value=""></option>
                                <option value="Opcao">Opção de quarto 1</option>
                                <option value="Opcao">Opção de quarto 2</option>
                                <option value="Opcao">Opção de quarto 3</option>
                                <option value="Opcao">Opção de quarto 4</option>
                                <option value="Opcao">Opção de quarto 5</option>
                            </select>

                            <label for="number_of_children">Pussui filhos?</label>
                            <select name="text" id="number_of_children">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>

                            <label for="age" class="age">Faixa etária</label>
                            <select name="text" id="age" disabled>
                                <option value=""></option>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                                <option value="">5</option>
                            </select>
                        </div>
                        <a href="#status"class="btn btn_next upp center top32">Avançar</a>
                </div>
                <div class="tab tab_2 hide" tab="2">
                   <h3 class="title center">Qual desses estilos você tem em mente para decorar o seu ambiente? Selecione quantos quiser</h3>
                    <div class="list">
                        <div class="item">
                            <label for="tema_1">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_1' id="tema_1"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_2">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_2' id="tema_2"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_3">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_3' id="tema_3"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_4">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_4' id="tema_4"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_5">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_5' id="tema_5"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_6">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_6' id="tema_6"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_7">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_7' id="tema_7"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_8">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_8' id="tema_8"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="tema_9">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='tema_9' id="tema_9"/>
                            <p class="name">Nome do estilo</p>
                        </div>
                    </div>
                    <a href="#status"class="btn btn_next upp center top32">Avançar</a>
                </div>
                <div class="tab tab_3 hide" tab="3">
                   <h3 class="title center">Qual desses ambientes têm a sua cara? Selecione quantos quiser</h3>
                    <div class="list">
                        <div class="item">
                            <label for="ambiente_1">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_1' id="ambiente_1"/>
                            <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_2">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_2' id="ambiente_2"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_3">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_3' id="ambiente_3"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_4">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_4' id="ambiente_4"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_5">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_5' id="ambiente_5"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_6">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_6' id="ambiente_6"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_7">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_7' id="ambiente_7"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_8">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_8' id="ambiente_8"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_9">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_9' id="ambiente_9"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_10">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_10' id="ambiente_10"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_11">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_11' id="ambiente_11"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                        <div class="item">
                            <label for="ambiente_12">
                                <span class="mask_item hide"><img class="check" src="" alt=""></span>
                                <img class="" src="<? echo $url?>assets/images/produto.jpg" alt="">
                            </label>
                            <input class="hide" type="checkbox" name='ambiente_12' id="ambiente_12"/>
                           <p class="name mobile_item">Nome do estilo</p>
                        </div>
                    </div>
                    <a href="#status"class="btn btn_next upp center top32">Avançar</a>
                </div>
                <div class="tab tab_4 hide" tab="4">
                   <h3 class="title center">Você tem referências que gostaria de mandar? <br> Suba aqui as suas próprias fotos.</h3>
                    <div class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                        <div class="dz-default dz-message">
                            <img class="icon" src="<? echo $url?>assets/images/icon/cloud.svg" alt="">
                            <button class="dz-button" type="button">Clique ou arraste para fazer upload</button>
                        </div>
                    </div>
                    <a href="#status"class="btn btn_next upp center top32">Avançar</a>
                </div>
                <div class="tab tab_5 hide" tab="5">
                   <h3 class="title center">Escolha o tipo de projeto mais adequado para você</h3>
                    <div class="box">
                        <label for="basic_plan_" class="basic_plan">
                            <span class="dot"><img class="icon hide" src="<? echo $url?>assets/images/icon/check.svg" alt=""></span>
                            <span class="title">Básico</span>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Moodboard</p>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Cartela de cores</p>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Montagem 2D</p>
                            <p>a partir de <span class="price"><br>R$ YY,00</span></p>
                        </label>
                        <input class="hide" type="checkbox" name='basic_plan' id="basic_plan"/>
                    </div>
                    <div class="box">
                        <label for="complete_plan_" class="complete_plan">
                            <span class="dot"><img class="icon hide" src="<? echo $url?>assets/images/icon/check.svg" alt=""></span>
                            <span class="title">Completo</span>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Planta e Planejamento</p>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Dossiê decorativo</p>
                            <p class="text"><img class="icon" src="<? echo $url?>assets/images/icon/check.svg" alt="">Renderização 3D</p>
                            <p>a partir de <span class="price"><br>R$ YY,00</span></p>

                        </label>
                        <input class="hide" type="checkbox" name='complete_plan' id="complete_plan"/>
                    </div>
                </div>
                <input class="btn upp center btn_send hide" type="submit" value="enviar">
            </form>
        </article>
   </section>
    <?php include 'assets/footer.php'; ?>
</body>
</html>