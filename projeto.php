<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="projeto">
    <?php include 'assets/header.php'; ?>
    <section class="slider_projeto">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
        <img class="" src="<? echo $url?>assets/images/Banner.jpg" alt="">
    </section>
    <section class="">
        <article class="center">
            <h1 class="main_title center">Projeto de decoração</h1>
            <p class="text center">Quer um projeto completo da Mama para a sua casa?</p>
            <p class="text center top32">Veja como é fácil:</p>
        </article>
    </section>
    <section>
        <article class="orcamento">
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/one.svg" alt="">
                <span>Entendendo seus gastos</span>
                <p>Primeiro você conta pra gente como é o ambiente que você quer decorar e quais são as suas inspirações</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/two.svg" alt="">
                <span>Aguarde nosso contato</span>
                <p>Depois de escolher se você vai querer o projeto básico ou completo, entraremos em contato com você por e-mail</p>
            </div>
            <div class="item">
                <img class="img_item" src="<? echo $url?>assets/images/icon/draw/three.svg" alt="">
                <span>Receba seu projeto</span>
                <p>Um arquiteto especializado vai montar um projeto de decoração completo com base nos seus gostos</p>
            </div>
            <div>
                <a href="./mood.php" class="btn upp">Eu quero</a>
            </div>
        </article>
    </section>
    <section class="projects">
        <article class="item">
            <div>
                <h3 class="title">Projeto Básico</h3>
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="text">Moodboard com ideias de decoração selecionadas por especialistas em design de interiores.
                    <br>
                    Cartela de cores para você usar como referência na hora de escolher todos os produtos para o seu ambiente.
                    <br>
                    Montagem 2D do seu ambiente com a decoração para você visualizar como ficará o seu projeto.
                </p>
                <p class="price">a partir de <span class="value">R$ YY,00</span></p>
            </div>
            <a href="./mood.php" class="btn upp">Eu quero!</a>
        </article>
        <article class="item top64">
            <div>
                <h3 class="title">Projeto Completo</h3>
                <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="text">
                    Planta do ambiente com o planejamento completo do layout do ambiente. <br>
                    Dossiê decorativo completo com paleta de cores, referências, indicação de produtos e composição do quarto.
                    <br>
                    Renderização 3D do seu ambiente feita por um arquiteto especializado com a decoração para você visualizar fielmente como ficará o seu projeto.
                </p>
                <p class="price">a partir de <span class="value">R$ YY,00</span></p>
            </div>
            <a href="./mood.php" class="btn upp">Eu quero!</a>
        </article>
    </section>


    <?php include 'assets/footer.php'; ?>
</body>
</html>