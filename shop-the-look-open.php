<!DOCTYPE HTML>
<html>
	<head>
	<?php 
		include 'assets/config.php';
 
		$tituloPagina = ''. $empresa;
		$descricaoCompartilhamento = '';
		
		$urlPagina = $url;
		$imagemCompartilhamento = $images.'anuncio_demanda_08.png';
		$sessao = 'home';
	?>
    <title><? echo $tituloPagina ?></title>
	<?php include 'assets/head.php'; ?>

		<meta property="og:title" content="<? echo $tituloPagina ?>" />
		<meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
		<meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

	</head>
	<body class="shop_the_look_open" >
    <?php include 'assets/header.php'; ?>
    <section>
        <article>
            <div class="selected_category">
                <a class="link" href="\categoria" >
                    <img class="icon icon_left" src="<? echo $url?>assets/images/icon/arrowleft.svg" alt="">
                    Papel de parede adesivo
                </a>
            </div>
            <div class="selected_product">
                <div class="gallery">
                    <div class="services-slider">
                        <div class="main-container">
                            <div class="slider slider-main">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="nav-container">
                            <div class="slider-nav">
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                                <div>
                                    <img class="img" src="<? echo $url?>assets/images/produto.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="options">
                    <div class="divider mobile_item"></div>
                    <h3 class="title" id="title_produto">Nome do produto ocupando até três linhas</h3>
                    <p class="center question">Qual é o tamanho da sua parede?</p>
                    <div id="calc" class="calc">
                        <div class="item">
                            <label for="">Largura (cm)</label>
                            <input type="text"> <br>
                        </div>
                        <div class="item">
                            <label for="">Altura (cm)</label>
                            <input type="text">
                        </div>
                        <div>
                            <button class="btn center upp"> Calcular</button>
                        </div>
                    </div>
                    <div class="divider top32 bot32"></div>
                    <ul class="shop_list">
                        <li class="item">
                            <p class="name">Nome do produto ocupando até duas linhas</p>
                            <p class="value center">RS YY,00</p>
                        </li>
                        <li class="item">
                            <p class="name">Nome do produto ocupando até duas linhas</p>
                            <p class="value center">RS YY,00</p>
                        </li>
                        <li class="item">
                            <p class="name">Nome do produto ocupando até duas linhas</p>
                            <p class="value center">RS YY,00</p>
                        </li>
                        <li class="item">
                            <p class="name">Nome do produto ocupando até duas linhas</p>
                            <p class="value center">RS YY,00</p>
                        </li>
                    </ul>
                    <div class="divider top32 bot32"></div>
                    <p class="value center">RS YY,00</p>
                    <p class="installments center bot32">em até 3x de R$ XX,00 sem juros
                        ou R$ XX,00 no boleto bancário
                    </p>
                    <button class="btn center upp">Adicionar ao carrinho</button>

                </div>
            </div>
        </article>
    </section>
    <?php include 'assets/footer.php'; ?>
	</body>
</html>