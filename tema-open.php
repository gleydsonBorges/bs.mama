<!DOCTYPE HTML>
<html>
<head>
    <?php
    include 'assets/config.php';

    $tituloPagina = ''. $empresa;
    $descricaoCompartilhamento = '';

    $urlPagina = $url;
    $imagemCompartilhamento = $images.'anuncio_demanda_08.png';
    $sessao = 'home';
    ?>
    <title><? echo $tituloPagina ?></title>
    <?php include 'assets/head.php'; ?>

    <meta property="og:title" content="<? echo $tituloPagina ?>" />
    <meta name="description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO SEO GOOGLE -->
    <meta property="og:description" content="<? echo $descricaoCompartilhamento ?>" /> <!-- DESCRIÇÃO FACEBOOK -->

</head>
<body class="categoria tema_open">
    <?php include 'assets/header.php'; ?>
    <section class="produtos">
        <article class="">
            <div class="selected_category">
                <a class="link" href=".\temas.php">
                    <img class="icon icon_left" src="http://mama.ko/assets/images/icon/arrowleft.svg" alt="">
                    Temas
                </a>
            </div>
            <h1 class="main_title center">Nome do Tema</h1>
            <div class="center">
                <button class="btn_filter mobile_item">Filtrar<img class="icon" src="<? echo $url?>assets/images/icon/filter.svg" alt=""></button>
                <button class="btn_order mobile_item">Ordenar<img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
            </div>
            <div class="nav_drop">
                <div class="close_navdrop mobile_item hide"><img src="http://mama.ko/assets/images/icon/close.svg" alt=""></div>
                <div class="dropdown drop_5">
                    <button class="btn_drop"><span>Preço</span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list">
                            <li class="item"><input type="range" min="0" max="1000" value="400" step="10" class="slider" id="range_prod"></li>
                            <li class="item"><p class="price">Até: R$ <span id="value"></span>,00</p></li>
                        </ul>
                    </div>
                </div>
                <img class="img bar hide" src="<? echo $url?>assets/images/icon/bar.svg" alt="">
                <div class="dropdown drop_6 ">
                    <button class="btn_drop"><span>Ordenar </span><img class="icon" src="<? echo $url?>assets/images/icon/arrowdown.svg" alt=""></button>
                    <div class="drop_content hide">
                        <ul class="list order">
                            <li class="item"><span class="dot checked"></span>Ordem alfabética</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Mais vendidos</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Menos preço</li>
                            <li class="item"><span class="dot" style="background-color: "></span>Maior preço</li>
                        </ul>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section>
        <article class="grid products">
           <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
            <a class="item" href="./produto.php">
                <img class="img responsive" src="<? echo $url?>assets/images/produto.jpg" alt="">
                <p class="category">Nome do produto ocupando até duas linhas</p>
                <ul class="list_colors">
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                    <li class="dot" style="background-color: "></li>
                </ul>
                <span class="on_sale hide"> R$ XX,00</span>
                <span class="price">R$ XX,00</span>
            </a>
        </article>
        <div class="pagination">
            <div class="center">
                <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowleft" alt=""></a>
                <a href="#" class="active">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#"><img class="icon" src="<? echo $url?>assets/images/icon/arrowright" alt=""></a>
            </div>
        </div>
    </section>
    <section>
        <article class="custom">
            <h5 class="title">Faça seu produto personalizado!</h5>
            <p class="center">Não encontrou a estampa, cor ou tamanho que queria?</p>
            <p class="center">Manda uma mensagem pra gente, vamos adorar criar <br> um produto especial só pra você!</p>
            <form action="#" class="form top32">
                <label for="name">Nome</label>
                <input type="text" id="name" name="name" placeholder="">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" placeholder="">
                <label for="subject">Mensagem</label>
                <textarea id="subject" name="subject" placeholder="" style="height:200px"></textarea>
                <input class="btn upp center top32" type="submit" value="mandar mensagem">
            </form>
        </article>
    </section>
    <?php include 'assets/footer.php'; ?>
</body>
</html>